# -*- coding: utf-8 -*-

import json
import datetime

import bottle


def patch_json_plugin(app):

    class MyJsonEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, datetime.datetime):
                return str(obj.strftime("%Y-%m-%d %H:%M:%S"))
            return json.JSONEncoder.default(self, obj)

    for plugin in app.plugins:
        if isinstance(plugin, bottle.JSONPlugin):
            plugin.json_dumps = lambda s: json.dumps(s, cls=MyJsonEncoder)
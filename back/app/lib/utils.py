# -*- coding: utf-8 -*-
# #  Bottle methods  # #
from bottle import request

import logging
logger = logging.getLogger('magic_stone')
hdlr = logging.FileHandler('magic_stone.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

def postd():
    return request.forms


def post_get(name, default=''):
    return request.POST.get(name, default).strip()


def json_get(name, default=''):
    return request.json.get(name, default)


def parse_return(succes, data):
    if succes:
        status = "success"
    else:
        status = "error"
    return {'status': status, 'data': data}
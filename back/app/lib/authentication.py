# -*- coding: utf-8 -*-
from cork import Cork
from cork.mongodb_backend import MongoDBBackend

# Get a connection to the MongoDb database
# This is instead of the built-in JsonBackend
backend = MongoDBBackend(
    hostname="localhost",
    db_name="magic_stone",
    initialize=False,
)

# Start Cork!
# Cork will use the default Bottle app automatically
# aaa = Cork(backend, email_sender='email@example.com', smtp_server='mysmtpserver')
aaa = Cork(backend=backend, email_sender='yuri.piratello@gmail.com', smtp_server='smtp.gmail.com')
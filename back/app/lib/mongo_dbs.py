# -*- coding: utf-8 -*-

from pymongo import Connection
con = Connection()

# collection
# {
#     'username':'yuripiratello',
#     'cards':{
#         'id_card':'1',
#         'amount':'5,50',
#         'quantity':15,
#         'edition':'RTR',
#         ...
#     }
# }
collections = con.magic_stone.collections
want = con.magic_stone.want
users = con.magic_stone.users
# -*- coding: utf-8 -*-
import re
from bottle import post, response, get, request
from ..lib.authentication import aaa
from ..lib.mongo_dbs import collections, want
from ..lib.utils import parse_return, json_get, logger


def add_card_collection(username, card):
    coll = collections.find_one({'username': username})
    if not coll:
        collections.insert({'username': username, 'cards': []})
    coll = collections.find_one(
        {
            'username': username,
            'cards.id_card': card['id_card'],
            'cards.edition': card['edition'],
            'cards.foil': card['foil']
        }
    )
    if not coll:
        collections.update({'username': username},
                           {'$push': {
                               'cards': {
                                   'id_card': card['id_card'],
                                   'edition': card['edition'],
                                   'quantity': 0,
                                   'foil': card['foil']
                               }
                           }})
    key = {
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card['foil']
    }
    data = {
        '$set': {
            'cards.$.id_card': card['id_card'],
            'cards.$.amount': card.get('amount', 0),
            'cards.$.edition': card['edition'],
            'cards.$.trade': card.get('trade', False),
            'cards.$.sell': card.get('sell', False),
            'cards.$.foil': card('foil', False),
        },
        '$inc': {
            'cards.$.quantity': card['quantity']
        }
    }
    collections.update(key, data)
    logger.info('%s cartas %s - %s adicionada(s) a coleção do usuario %s .' %
                (str(card['quantity']), card['edition'], str(card['id_card']), username))
    return True


def sub_card_collection(username, card):
    coll = collections.find_one({'username': username})
    if not coll:
        collections.insert({'username': username, 'cards': []})
        raise Exception("Você não possui esta carta")
    coll = collections.find_one({
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card['foil']
    })
    if not coll:
        Exception("Você não possui esta carta")
    key = {
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card['foil']
    }
    data = {
        '$inc': {
            'cards.$.quantity': card['quantity'] * -1
        }
    }
    collections.update(key, data)
    logger.info('%s cartas %s - %s removida(s) da coleção do usuario %s .' %
                (str(card['quantity']), card['edition'], str(card['id_card']), username))
    return True


def add_card_want(username, card):
    coll = want.find_one({'username': username})
    if not coll:
        want.insert({'username': username, 'cards': []})

    coll = want.find_one({
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card.get('foil', False)
    })
    if not coll:
        want.update(
            {'username': username},
            {'$push': {
                'cards': {
                    'id_card': card['id_card'],
                    'edition': card['edition'],
                    'name_pt': card['name_pt'],
                    'name_en': card['name_en'],
                    'quantity': 0,
                    'foil': card.get('foil', False)
                }
            }})
    key = {
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card['foil']
    }
    data = {
        '$set': {
            'cards.$.trade': card.get('trade', True),
            'cards.$.buy': card.get('buy', True),
        },
        '$inc': {
            'cards.$.quantity': card['quantity']
        }
    }
    want.update(key, data)
    logger.info('%s cartas %s - %s adicionada(s) a want do usuario %s .' %
                (str(card['quantity']), card['edition'], str(card['id_card']), username))
    return True


def sub_card_want(username, card):
    coll = want.find_one({'username': username})
    if not coll:
        want.insert({'username': username, 'cards': []})
        raise Exception("Você não possui esta carta")
    coll = want.find_one({
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'name_pt': card['name_pt'],
        'name_en': card['name_en'],
        'cards.foil': card['foil']
    })
    if not coll:
        raise Exception("Você não possui esta carta")
    key = {
        'username': username,
        'cards.id_card': card['id_card'],
        'cards.edition': card['edition'],
        'cards.foil': card['foil']
    }
    data = {
        '$inc': {
            'cards.$.quantity': card['quantity'] * -1
        }
    }
    collections.update(key, data)
    logger.info('%s cartas %s - %s removida(s) da want do usuario %s .' %
                (str(card['quantity']), card['edition'], str(card['id_card']), username))
    return True


def get_collection(username):
    coll = collections.find_one({'username': username})
    return coll


def get_want(username):
    wt = want.find_one({'username': username})
    return wt


def find_card_in_collections(name, edition, foil):
    return collections.find({
        'cards.edition': edition,
        'cards.foil': foil,
        'name_pt': {'$regex': re.compile(name)},
        'name_en': {'$regex': re.compile(name)}
    })


@post('/api/cards/collection/add/')
@post('/api/cards/collection/add')
def add_card_to_collection():
    response.content_type = 'application/json'
    try:
        card = json_get('card', None)
        if card:
            if add_card_collection(aaa.current_user.username, card, 'collection'):
                return parse_return(True, 'Carta adicionada com sucesso.')
            else:
                return parse_return(True, 'Não foi possível adicionar a carta.')
        else:
            return parse_return(False, 'Parâmetro carta não encontrado.')
    except Exception as e:
        return parse_return(False, e.message)


@post('/api/cards/want/add/')
@post('/api/cards/want/add')
def add_card_to_want():
    response.content_type = 'application/json'
    try:
        card = json_get('card', None)
        if card:
            if add_card_want(aaa.current_user.username, card):
                return parse_return(True, 'Carta adicionada com sucesso.')
            else:
                return parse_return(True, 'Não foi possível adicionar a carta.')
        else:
            return parse_return(False, 'Parâmetro carta não encontrado.')
    except Exception as e:
        return parse_return(False, e.message)


@post('/api/cards/collection/sub/')
@post('/api/cards/collection/sub')
def sub_card_to_collection():
    response.content_type = 'application/json'
    try:
        card = json_get('card', None)
        if card:
            if sub_card_collection(aaa.current_user.username, card):
                return parse_return(True, 'Carta removida com sucesso.')
            else:
                return parse_return(True, 'Não foi possível adicionar a carta.')
        else:
            return parse_return(False, 'Parâmetro carta não encontrado.')
    except Exception as e:
        return parse_return(False, e.message)


@post('/api/cards/want/sub/')
@post('/api/cards/want/sub')
def sub_card_to_want():
    response.content_type = 'application/json'
    try:
        card = json_get('card', None)
        if card:
            if sub_card_want(aaa.current_user.username, card):
                return parse_return(True, 'Carta removida com sucesso.')
            else:
                return parse_return(True, 'Não foi possível adicionar a carta.')
        else:
            return parse_return(False, 'Parâmetro carta não encontrado.')
    except Exception as e:
        return parse_return(False, e.message)


@get('/api/cards/collection/:username/')
@get('/api/cards/collection/:username')
def get_collection(username):
    response.content_type = 'application/json'
    if username:
        collection = get_collection(username)
        if collection:
            return parse_return(True, collection)
        else:
            return parse_return(False, 'Coleção não encontrada.')
    else:
        return parse_return(False, 'Parâmetro não encontrado.')


@get('/api/cards/want/:username/')
@get('/api/cards/want/:username')
def get_collection(username):
    response.content_type = 'application/json'
    if username:
        wt = get_want(username)
        if wt:
            return parse_return(True, wt)
        else:
            return parse_return(False, 'Want não encontrada.')
    else:
        return parse_return(False, 'Parâmetro não encontrado.')


@get('/api/cards/find/')
@get('/api/cards/find')
def find_cards():
    response.content_type = 'application/json'
    return parse_return(True, find_card_in_collections(request.GET.get('name', ''), request.GET.get('edition', ''),
                                                       request.GET.get('foil', '')))
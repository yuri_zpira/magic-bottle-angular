# -*- coding: utf-8 -*-
from bottle import route, request, post
from ..lib.authentication import aaa
from ..lib.utils import post_get, parse_return, postd
from ..lib.mongo_dbs import users


@post('/api/users/login')
def login():
    """Authenticate users"""
    username = post_get('username')
    password = post_get('password')
    sess = request.environ.get('beaker.session')
    sess['user_profile'] = users.find_one({'username': username})
    # aaa.login(username, password, success_redirect='/', fail_redirect='/login')
    if aaa.login(username, password):
        return parse_return(True, {
            'username': aaa.current_user.username,
            'email': aaa.current_user.email_addr,
            'role': aaa.current_user.role,
            'level': aaa.current_user.level,
            'profile': sess['user_profile']
        })
    else:
        return parse_return(False, 'Usuário ou senha incorreto.')


@route('/api/users/user_is_anonymous')
def user_is_anonymous():
    if aaa.user_is_anonymous:
        return 'True'
    return 'False'


@route('/api/users/logout')
def logout():
    aaa.logout()
    sess = request.environ.get('beaker.session')
    sess.delete()
    return parse_return(True, '')


@post('/api/users/register')
def register():
    """Send out registration email"""
    template_email = '/home/yuri/Documentos/Projetos/magic-bottle-angular/back/app/lib/cork_emails/registration_email.tpl'
    aaa.register(post_get('username'), post_get('password'), post_get('email_address'), email_template=template_email)
    return 'Please check your mailbox.'


@route('/api/users/validate_registration/:registration_code')
def validate_registration(registration_code):
    """Validate registration, create user account"""
    aaa.validate_registration(registration_code)
    return 'Thanks. <a href="/login">Go to login</a>'


@post('/api/users/reset_password')
def send_password_reset_email():
    """Send out password reset email"""
    aaa.send_password_reset_email(
        username=post_get('username'),
        email_addr=post_get('email_address')
    )
    return 'Please check your mailbox.'


@route('/api/users/change_password/:reset_code')
def change_password(reset_code):
    """Show password change form"""
    return dict(reset_code=reset_code)


@post('/api/users/change_password')
def change_password():
    """Change password"""
    aaa.reset_password(post_get('reset_code'), post_get('password'))
    return 'Thanks. <a href="/login">Go to login</a>'


@route('/my_role')
def show_current_user_role():
    """Show current user role"""
    session = request.environ.get('beaker.session')
    print "Session from simple_webapp", repr(session)
    aaa.require(fail_redirect='/login')
    return aaa.current_user.role


# Admin-only pages

@route('/admin')
def admin():
    """Only admin users can see this"""
    aaa.require(role='admin', fail_redirect='/sorry_page')
    return dict(
        current_user=aaa.current_user,
        users=aaa.list_users(),
        roles=aaa.list_roles()
    )


@post('/api/users/create_user')
def create_user():
    try:
        aaa.create_user(postd().username, postd().role, postd().password)
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)


@post('/api/users/delete_user')
def delete_user():
    try:
        aaa.delete_user(post_get('username'))
        return dict(ok=True, msg='')
    except Exception, e:
        print repr(e)
        return dict(ok=False, msg=e.message)


@post('/create_role')
def create_role():
    try:
        aaa.create_role(post_get('role'), post_get('level'))
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)


@post('/delete_role')
def delete_role():
    try:
        aaa.delete_role(post_get('role'))
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)


@post('/api/users/current')
def login():
    aaa.require(fail_redirect='/login')
    sess = request.environ.get('beaker.session')
    return parse_return(True, {
        'username': aaa.current_user.username,
        'email': aaa.current_user.email_addr,
        'role': aaa.current_user.role,
        'level': aaa.current_user.level,
        'profile': sess['user_profile']
    })
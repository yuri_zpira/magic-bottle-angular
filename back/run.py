# -*- coding: utf-8 -*-
from beaker.middleware import SessionMiddleware
import bottle
from app.lib import patches

# from app.libs import session, authentication, patches
from app.api import users, cards
# from app.api import users, login, referencia, desempenho, network, survey, \
#     public, bonus, downline, credito, relatorios_de_rede, loja

# Cork uses Beaker for sessions.
# Define how we want our sessions
session_opts = {
    'session.type': 'cookie',
    'session.validate_key': True,
    'session.cookie_expires': True,
    'session.timeout': 3600 * 24, # 1 day
    'session.encrypt_key': 'please use a random key and keep it secret!',
}

app = bottle.app()

# patches
patches.patch_json_plugin(app)

# Setup Beaker middleware to handle sessions and cookies
app = SessionMiddleware(app, session_opts)

# Start the Bottle webapp
bottle.run(app=app, reloader=True)

if __name__ == '__main__':
    bottle.run(app=app, host='localhost', port=8070, reloader=True, debug=True)
else:
    application = app
# -*- coding: utf-8 -*-
from bottle import route, static_file, error


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='../front/app/')


@error(404)
def error404(error):
    return 'Nothing here, sorry'
(function($) {

    $.easing.easeInOutExpo = function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    };


    window.UPutils = {

        qualifications: [
            {'name': 'Especialista', slug: 'especialista'},
            {'name': 'Bronze', slug: 'bronze'},
            {'name': 'Prata', slug: 'prata'},
            {'name': 'Ouro', slug: 'ouro'},
            {'name': 'Rubi', slug: 'rubi'},
            {'name': 'Diamante', slug: 'diamante'},
            {'name': 'Duplo Diamante', slug: 'duplo_diamante'},
            {'name': 'Triplo Diamante', slug: 'triplo_diamante'},
            {'name': 'Ônix', slug: 'onix'}
        ],

        formatNumber: function(value, decimals) {
            if (!decimals) decimals = 0;
            value = parseFloat(value).toFixed(decimals);

            var parts = value.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return parts.join(",");
        },

        realValNumber: function(value) {
            return parseFloat(
                value
                    .toString()
                    .replace(/\./g, '')
                    .replace(',', '.')
            );
        },

        animateTo: function (el, to_val, speed, formatFunc, callback) {
            if (!formatFunc)
                formatFunc = function(a) {return a;};

            $({countNum: UPutils.realValNumber(el.text())})
                .animate({countNum: to_val}, {
                    duration: speed,
                    easing:'easeInOutExpo',
                    step: function() {
                        el.text(formatFunc(this.countNum));
                },
                complete: function() {
                    el.text(formatFunc(this.countNum));
                    if (callback) callback();
                }
            });
        },

        setBackGroundHorizontalGradient: function(el, start_color, end_color) {
            var $el = $(el),
                original_bg = $el.css('background');

            var vendor_vals = [
                'linear-gradient(to right,  {color_start} 0%,{color_end} 100%)',
                '-webkit-linear-gradient(left,  {color_start} 0%,{color_end} 100%)',
                '-ms-linear-gradient(left,  {color_start} 0%,{color_end} 100%)',
                '-moz-linear-gradient(left,  {color_start} 0%, {color_end} 100%)',
                '-webkit-gradient(linear, left top, right top, color-stop(0%,{color_start}), color-stop(100%,{color_end}))',
                '-o-linear-gradient(left,  {color_start} 0%,{color_end} 100%)'
            ];

            for (var vendor_val_idx in vendor_vals) {
                var vendor_val = vendor_vals[vendor_val_idx]
                    .replace('{color_start}', start_color)
                    .replace('{color_end}', end_color);

                $el.css('background', vendor_val);
                if ($el.css('background') !== original_bg)
                    return true;
            }
            return false;
        },

        getNestedProp: function(obj, propString, fallback) {

            if (!propString) return obj;
            var prop, props = propString.split('.');

            //for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
            for (var i = 0, iLen = props.length - 1; i <= iLen; i++) {
                prop = props[i];

                if (typeof obj == 'object' && obj !== null && prop in obj){
                    obj = obj[prop];
                }
                else
                    return fallback;
            }

            //return obj[props[i]];
            return obj;
        },
    }

})($);
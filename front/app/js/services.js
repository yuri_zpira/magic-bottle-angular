'use strict';

/* Services */

angular.module('magicStone.services', ['ngResource'])
    .factory('AlertService', ['$rootScope', '$timeout',
        function ($rootScope, $timeout) {
            var alertService = {};

            // create an array of alerts available globally
            $rootScope.alerts = [];
            $rootScope.alerts_modal = [];

            alertService.add = function (type, msg, persistente, modal) {
                if (modal) {
                    $rootScope.alerts_modal.push({'type': type, 'msg': msg, 'persistente': persistente || false});
                    $timeout(function () {
                        for (var a = $rootScope.alerts_modal.length; a <= $rootScope.alerts_modal.length && a >= 0; a--) {
                            if ($rootScope.alerts_modal[a] !== undefined) {
                                if ($rootScope.alerts_modal[a].msg == msg && $rootScope.alerts_modal[a].type == type) {
                                    $rootScope.alerts_modal.splice(a, 1);
                                }
                            }
                        }
                    }, 5000);
                } else {
                    $rootScope.alerts.push({'type': type, 'msg': msg, 'persistente': persistente || false});
                    $timeout(function () {
                        for (var a = $rootScope.alerts.length; a <= $rootScope.alerts.length && a >= 0; a--) {
                            if ($rootScope.alerts[a] !== undefined) {
                                if ($rootScope.alerts[a].msg == msg && $rootScope.alerts[a].type == type) {
                                    $rootScope.alerts.splice(a, 1);
                                }
                            }
                        }
                    }, 4000);
                }
            };

            alertService.getAll = function () {
                return $rootScope.alerts;
            };

            alertService.closeAlert = function (index) {
                $rootScope.alerts.splice(index, 1);
                $rootScope.alerts_modal.splice(index, 1);
            };

            alertService.clearTemporarios = function () {
                for (var a = $rootScope.alerts.length; a <= $rootScope.alerts.length && a >= 0; a--) {
                    if ($rootScope.alerts[a] !== undefined) {
                        if (!$rootScope.alerts[a].persistente) {
                            $rootScope.alerts.splice(a, 1);
                        }
                    }
                }
                for (var a = $rootScope.alerts_modal.length; a <= $rootScope.alerts_modal.length && a >= 0; a--) {
                    if ($rootScope.alerts_modal[a] !== undefined) {
                        if (!$rootScope.alerts_modal[a].persistente) {
                            $rootScope.alerts_modal.splice(a, 1);
                        }
                    }
                }
            };

            alertService.alertType = function (type) {
                if (type == 'success')
                    return 'alert-success'
                else if (type == 'info')
                    return 'alert-info'
                else if (type == 'error')
                    return 'alert-error'
                else
                    return 'alert-info'
            };

            return alertService;
        }])
    .factory('User', [
        '$http', 
        function ($http) {
            return {
                'login': function(loginData){
                    return $http.post('/api/users/login', loginData);
                }
            }
        }
    ])
    .factory('CardList', [
        '$resource', function ($resource) {
            return $resource('/api/cardlists/:cardlistId/:action', {
                cardlistId: '@_id'
            }, {});
        }
    ])
    .factory('Card', [
        '$resource', function ($resource) {
            return $resource('/api/cards/:cardId/:action', {
                cardId: '@_id'
            });
        }
    ]);

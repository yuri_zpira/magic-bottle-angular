angular.module('magicStone').controller('loginCtrl', [
    'User', '$scope', 
    function(User, $scope){
        $scope.login = function() {
			User.login($scope.loginData)
				.success(function(data, status, headers, config) {
					debugger;
					if (data.status == 'success') {
						// succefull login
						User.isLogged = true;
						User.username = data.username;
					}
					else {
						User.isLogged = false;
						User.username = '';
					}
				})
				.error(function(data, status, headers, config) {
					User.isLogged = false;
					User.username = '';
				});
		}
    }]);